package med1

import "testing"

// TestHelloEmpty calls greetings.Hello with an empty string,
// checking for an error.
func TestFactorial(t *testing.T) {
	if Factorial(5) != 120 {
		t.Error("Expected 120, got ", Factorial(5))
	}
	if Factorial(10) != 3628800 {
		t.Error("Expected 3628800, got ", Factorial(10))

	}
	if Factorial(1) != 1 {
		t.Error("Expected 1, got ", Factorial(1))
	}
	if Factorial(7) != 5040 {
		t.Error("Expected 5040, got ", Factorial(7))
	}
	if Factorial(0) != 1 {
		t.Error("Expected 1, got ", Factorial(0))
	}
	t.Log("TestFactorial() passed")
}

func TestPermutations(t *testing.T) {
	if Permutations(5, 3) != 60 {
		t.Error("Expected 60, got ", Permutations(5, 3))
	}
	if Permutations(10, 5) != 30240 {
		t.Error("Expected 30240, got ", Permutations(10, 5))
	}
	if Permutations(1, 1) != 1 {
		t.Error("Expected 1, got ", Permutations(1, 1))
	}
	if Permutations(7, 3) != 210 {
		t.Error("Expected 210, got ", Permutations(7, 3))
	}
	if Permutations(0, 0) != 1 {
		t.Error("Expected 1, got ", Permutations(0, 0))
	}
	t.Log("TestPermutations() passed")
}

func TestGCD(t *testing.T) {
	if GCD(13, 5) != 1 {
		t.Error("Expected 1, got ", GCD(13, 5))
	}
	if GCD(10, 5) != 5 {
		t.Error("Expected 5, got ", GCD(10, 5))
	}
	if GCD(1, 1) != 1 {
		t.Error("Expected 1, got ", GCD(1, 1))
	}
	if GCD(7, 3) != 1 {
		t.Error("Expected 1, got ", GCD(7, 3))
	}
	if GCD(0, 0) != 0 {
		t.Error("Expected 0, got ", GCD(0, 0))
	}
	t.Log("TestGCD() passed")
}
